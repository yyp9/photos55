package model;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import javafx.scene.image.Image;

/**Photo class, contains a list of tags for the photo, caption, date, and directory path
 * @author Preston Peng
 * @author Yug Patel
 *
 */
public class Photo implements java.io.Serializable {

	public ArrayList<String> tags;
	private String caption;
	public Calendar cal = Calendar.getInstance();
	private String location;
	
	/**Creates a photo
	 * @param loc location of the file
	 * @param fiName name of the photo
	 * @throws FileNotFoundException
	 */
	public Photo(String loc, String fiName) throws FileNotFoundException {
		tags = new ArrayList<String>();
		location = "" + loc;
		cal.set(Calendar.MILLISECOND, 0);
		caption = "[Default Caption]: " + fiName.substring(0, fiName.indexOf('.'));
		
	}
	
	/**
	 * @return photo caption
	 */
	public String getCaption() {
		return caption;
	}

	/**Sets caption
	 * @param cap caption to be added
	 */
	public void setCaption(String cap) {
		caption = "" + cap;
	}

	/**
	 * @returns photo tags
	 */
	public ArrayList<String> getTags(){
		return tags;
	}

	/**Adds new tags
	 * @param type Tag type
	 * @param value Tag value
	 */
	public void addNewTag(String type, String value) {
		tags.add(type + ": " + value);
	}
	
	/**
	 * @return date of photo
	 */
	public Calendar getDateTime() {
		return cal;
	}

	/**
	 * @return photo directory path
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @param loc sets directory path
	 */
	public void setLocation(String loc) {
		location = loc;
	}
}
