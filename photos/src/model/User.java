package model;

import java.util.ArrayList;

/**User class, containing a list of albums, a list of all tags from all albums, a list of all photos
 * @author Preston Peng
 * @author Yug Patel
 *
 */
public class User implements java.io.Serializable{
	
	private String userName;
	public ArrayList<Album> albums = new ArrayList<Album>();
	public ArrayList<String> globalTagTypes = new ArrayList<String>();
	public ArrayList<Photo> globalPhotos = new ArrayList<Photo>();
	
	/**Creates a user
	 * @param uName Username
	 */
	public User(String uName) {
		this.globalTagTypes.add("Location:");
		this.globalTagTypes.add("Person:");
		userName = uName;
	}
	
	/**
	 * @return user's username
	 */
	public String getUserName() {
		return userName;
	}
	
	/**
	 * @param a album to be added to user
	 */
	public void addNewAlbum(Album a) {
		albums.add(a);
	}
	
	/**
	 * @param ind index of album to be deleted
	 */
	public void deleteAlbum(int ind) {
		albums.remove(ind);
	}
	
}
