package model;

import java.util.ArrayList;

/**
 * Album class, contains a list of photos which it contains
 * @author Preston Peng
 * @author Yug Patel
 *
 */
public class Album implements java.io.Serializable {
	
	private String albumName;
	public ArrayList<Photo> photos;

	
	/**
	 * Creates a new instance of an album
	 * @param name indicates the name of the album
	 */

	public Album(String name) {
		photos = new ArrayList<Photo>();
		albumName = name;
	}
	/**Adds a photo to the album
	 * @param ph a photo to be added to the album
	 */	
	public void addNewPhoto(Photo ph) {
		photos.add(ph);
	}
	/**Removes a photo from the album
	 * @param ind index of a photo to be removed
	 */	

	
	/**
	 * @return album's name
	 */
	public String getAlbumName() {
		return albumName;
	}
	
	/**Removes a photo from the album
	 * @param ind index of a photo to be removed
	 */

	public void removePhoto(int ind) {
		photos.remove(ind);
	}
	/**Renames album
	 * @param newname new album name
	 */
	public void renameAlbum(String newname) {
		this.albumName = newname;
	}
	
	
	/**
	 * the modified to string version to return the name of the album
	 */
	public String toString() {
		return albumName;
	}
}
