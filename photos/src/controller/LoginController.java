package controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.stage.Stage;
import model.Album;
import model.Photo;
import model.User;




/**
 * @author Yug Patel
 * @author Preston Peng
 * This class is the controller class for the loginScreen and handles
 * functions that relate to the login screen
 *
 */
public class LoginController implements java.io.Serializable{
	
	/**
	 * FXML Button for logging in
	 */
	@FXML Button loginButton; 
	/**
	 * FXML TextField for entering username
	 */
	@FXML TextField loginField;
	
	
	/**
	 * The list of users that are registered
	 */
	public ArrayList<User> users = new ArrayList<User>();
	
	/**
	 * This function starts the login process by creating a serializing file if there isn't one already
	 * and loading the defauly stock user into the machine as well as the serializing file.
	 * If file available, this will deserialize and load users from that file
	 * @param mainStage the stage showing the scene
	 * @throws IOException if serializing file is not found
	 * @throws ClassNotFoundException if serialzing file is not found
	 */
	public void start(Stage mainStage) throws IOException, ClassNotFoundException {
		
		File tempFile = new File("users.ser");
		boolean exists = tempFile.exists();
		if(!exists) {
			User stock = new User("stock");
			stock.albums.add(new Album("stock"));
			stock.albums.get(0).photos.add(new Photo("file:/../data/rickroll.gif", "rickroll.gif"));			
			stock.albums.get(0).photos.add(new Photo("file:/../data/mikewoz.jpeg", "mikewoz.jpeg"));			
			stock.albums.get(0).photos.add(new Photo("file:/../data/mjcrying.jpeg", "mjcrying.jpeg"));			
			stock.albums.get(0).photos.add(new Photo("file:/../data/confused.jpeg", "confused.jpeg"));
			stock.albums.get(0).photos.add(new Photo("file:/../data/codeConfused.JPG", "codeConfused.JPG"));
			stock.albums.get(0).photos.add(new Photo("file:/../data/RUCSProfs.jpg", "RUCSProfs.jpg"));
			stock.albums.get(0).photos.add(new Photo("file:/../data/troll.jpeg", "troll.jpeg"));
			users.add(stock);
			serialize();
		}else {
			deserialize();
		}

	}
	
	/**
	 * This function takes the username from the textfield and checks to see if it is an admin username
	 * or a standard user username, and loads screens accordingly.
	 * @param e ActionEvent e signaling the user's clicking of the button
	 * @throws Exception if the admin or user class didn't successfully load
	 */
	@FXML void loginPressed(ActionEvent e) throws Exception {
	
		String user = loginField.getText();
		if(user.length() == 0) {
			
		}else if(user.equals("admin")){
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/AdminScreen.fxml"));
			Parent root = loader.load();
			AdminController adminControl = loader.getController();
			Stage stage = (Stage)((Node)e.getSource()).getScene().getWindow();
			adminControl.start(stage, users);
			stage.setScene(new Scene(root));
			stage.setTitle("Admin");
			stage.show();
			
		}else {
			boolean userExists = false;
			int i = 0;
			for(;i < users.size(); i++) {
				if(users.get(i).getUserName().equals(user)) {
					userExists = true;
					break;
				}
			}
			
			
			if(userExists) {
				FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/albumScreen.fxml"));
				Parent root = loader.load();
				UserController userControl = loader.getController();
				Stage stage = (Stage)((Node)e.getSource()).getScene().getWindow();
				User u = users.get(i);
				userControl.start(stage, u, users);
				stage.setScene(new Scene(root));
				stage.setTitle(u.getUserName() + "'s Albums");
				stage.show();
			}else {
				users.add(new User(user));
				FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/albumScreen.fxml"));
				Parent root = loader.load();
				UserController userControl = loader.getController();
				Stage stage = (Stage)((Node)e.getSource()).getScene().getWindow();
				User u = users.get(i);
				userControl.start(stage, u, users);
				stage.setScene(new Scene(root));
				stage.setTitle(u.getUserName() + "'s Albums");
				stage.show();
			}
		}
	}
	
	/**
	 * Quits app instantly after saving
	 * @throws IOException in case file for serializaiton is not found
	 */
	@FXML void quitApp() throws IOException {
		serialize();
		System.exit(0);
	}
	
	/**
	 * This method is used for serializing data for users
	 * @throws IOException in case the saving file isn't found
	 */
	public void serialize() throws IOException {
		FileOutputStream fout=new FileOutputStream(new File("users.ser"));    
		ObjectOutputStream out=new ObjectOutputStream(fout);    
		out.writeObject(users);
	}
	
	
	/**
	 * This function allows users to deserialize a file and load its contents into the 
	 * program allowing for data persistence
	 * @throws IOException in case the saving file isn't found
	 * @throws ClassNotFoundException in case the saving file won't load
	 */
	public void deserialize() throws IOException, ClassNotFoundException{
		ObjectInputStream in=new ObjectInputStream(new FileInputStream(new File("users.ser")));
		users = (ArrayList<User>)in.readObject();  
		in.close();  
	}
}
