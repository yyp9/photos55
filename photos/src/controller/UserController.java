package controller;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Optional;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.stage.Stage;
import model.Album;
import model.Photo;
import model.User;

/**
 * This class is the controller class for the album screen of the user and has functions
 * pertaining to that
 * @author Yug Patel
 * @author Preston Peng
 *
 */
public class UserController implements java.io.Serializable{
	
	/**
	 * the fxml listview that displays the list of albums along with the 
	 * number of photos and their date range
	 */
	@FXML ListView<String> albumListView;
	/**
	 * the observable list that holds the items to be displayed by the listview
	 */
	private ObservableList<String> obsList;
	/**
	 * the list of users that are registered on the app currently
	 */
	public ArrayList<User> users = new ArrayList<User>();
	/**
	 * The current user logged into the app
	 */
	private User currUser;
	
	/**
	 * This method loads up the album screen by displaying all the albums currently under the 
	 * logged in user that is currently on the app
	 * @param mainStage the stage where the scene is set
	 * @param currentUser the current user logged into the app
	 * @param userList the list of users currently registered on this app and device
	 */
	public void start(Stage mainStage, User currentUser, ArrayList<User> userList) {
		users = userList;
		currUser = currentUser;
		String range = "";
		obsList = FXCollections.observableArrayList();
		for(int i = 0; i < currUser.albums.size(); i++) {
			Album currA = currUser.albums.get(i);
			range = "";
			if(currA.photos.size()!= 0) {
			Photo leastP = currA.photos.get(0);
			for(int j = 1; j < currA.photos.size(); j++) {				
				Photo currP = currA.photos.get(j);
				if(leastP.cal.getTimeInMillis() > currP.cal.getTimeInMillis()) {
					leastP = currP;
				}
			}
			int lMonth = leastP.cal.get(Calendar.MONTH);
			int lday = leastP.cal.get(Calendar.DAY_OF_MONTH);
			int lyear = leastP.cal.get(Calendar.YEAR);
			Photo greatestP = currA.photos.get(0);
			for(int j = 1; j < currA.photos.size(); j++) {
				Photo currP = currA.photos.get(j);
				if(currP.cal.getTimeInMillis() > greatestP.cal.getTimeInMillis()) {
					greatestP = currP;
				}
			}
			int gMonth = greatestP.cal.get(Calendar.MONTH);
			int gday = greatestP.cal.get(Calendar.DAY_OF_MONTH);
			int gyear = greatestP.cal.get(Calendar.YEAR);
			range = "" + lMonth + "/" + lday + "/" + lyear + "-" + gMonth + "/" + gday + "/" + gyear;
		}
			obsList.add(currUser.albums.get(i).getAlbumName() + "\t\t# Photos: " + currUser.albums.get(i).photos.size() + "\t\t Range: " + range);
		}
		albumListView.setItems(obsList);
	}
	
	/**
	 * This function allows users to open a selected Album in a new screen that displays all the
	 * photos in a listView with their own separate functionality
	 * @param e ActionEvent signaling the user's clicking of the button
	 * @throws IOException if the loader file is not found
	 */
	@FXML void openAlbum(ActionEvent e) throws IOException {
		int currIndex = albumListView.getSelectionModel().getSelectedIndex();
		if(currIndex < 0) {
			showError("Please select an item");
		}else {
			Album a = currUser.albums.get(currIndex);
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/PhotosMainScreen.fxml"));
			Parent root = loader.load();
			AlbumController photosControl = loader.getController();
			Stage stage = (Stage)((Node)e.getSource()).getScene().getWindow();
			photosControl.start(stage, a, currUser, users);
			stage.setScene(new Scene(root));
			stage.setTitle("Photos");
			stage.show();
		}
	}
	
	/**
	 * This function allows users to create a new album by opening a dialog box that allows
	 * for entering the name of the album and later displaying it in the album listview
	 * @throws IOException in case file for serialization is not found
	 */
	@FXML void createNewAlbum() throws IOException {
		TextInputDialog dialog = new TextInputDialog("New Album");
		dialog.setTitle("Create New Album");
		dialog.setHeaderText("Enter Album Name: ");
		dialog.setContentText("Album");
		
		Optional<String> result = dialog.showAndWait();
		result.ifPresent(name -> {
			if(name.length() < 1) {
				showError("Invalid Album Name");
			}
			else if(!obsList.contains(name)) {
				obsList.add(name);
				currUser.albums.add(new Album(name));
			}else {
				showError("Album already Exists");
			}
//			for(int i = 0; i < currUser.albums.size(); i++) {
//				if(currUser.albums.get(i).getAlbumName().equals(name)) {
//					showError("Album already Exists");
//					return;
//				}
//			}
//			currUser.albums.add(new Album(name));
//			obsList.add(name);
			
		});
		serialize();
	}
	
	/**
	 * This function allows for the renaming of an existing selected album by opening a dialog box
	 * that requires user input and later updating the list accordingly with the name
	 * @throws IOException in case file for serialization is not found
	 */
	@FXML void renameAlbum() throws IOException {
		int currIndex = albumListView.getSelectionModel().getSelectedIndex();
		if(currIndex < 0) {
			showError("Please select an item");
			//return;
		}else {
			TextInputDialog dialog = new TextInputDialog("Rename Album");
			dialog.setTitle("Rename Album");
			dialog.setHeaderText("Enter Album Name: ");
			dialog.setContentText("Album");
			
			Optional<String> result = dialog.showAndWait();
			result.ifPresent(name -> {
				if(name.length() < 1) {
					showError("Invalid Album Name");
				}
				else if(!obsList.contains(name)) {
					currUser.albums.get(currIndex).renameAlbum(name);
					obsList.set(currIndex, name);
				}else {
					showError("Album with that name already exists!");
				}
				
			});
		}
		serialize();
		
	}
	
	/**
	 * This function allows for the deletion of the selected Album and all its contents
	 * @throws IOException in case file for serialization is not found
	 */
	@FXML void deleteAlbum() throws IOException {
		int currIndex = albumListView.getSelectionModel().getSelectedIndex();
		if(currIndex < 0) {
			showError("Please select an item");
			//return;
		}else {
			currUser.albums.remove(currIndex);
			obsList.remove(currIndex);
			
		}
		serialize();
	}
	
	/**
	 * Switches user to the photos search screen where they can search for photos
	 * across all albums through filters
	 * @param e ActionEvent signaling the user's clicking of the button
	 * @throws IOException in case loader file is not found
	 */
	@FXML void searchAlbums(ActionEvent e) throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/PhotoSearchScreen.fxml"));
		Parent root = loader.load();
		AlbumSearchController userControl = loader.getController();
		Stage stage = (Stage)((Node)e.getSource()).getScene().getWindow();
		userControl.start(stage, currUser, users);
		stage.setScene(new Scene(root));
		stage.show();
	}
	
	/**serializes current list, and returns to login page
	 * @param e event when button is pressed
	 * @throws IOException if the loader file is not found
	 * @throws ClassNotFoundException if the loader class is not found
	 */
	@FXML void userLogout(ActionEvent e) throws IOException, ClassNotFoundException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/loginScreen.fxml"));
		Parent root = loader.load();
		LoginController loginControl = loader.getController();
		Stage stage = (Stage)((Node)e.getSource()).getScene().getWindow();
		loginControl.start(stage);
		stage.setScene(new Scene(root));
		stage.setTitle("Login");
		stage.show();
	}
	
	/**
	 * Quits app instantly after saving
	 * @throws IOException in case file for serializaiton is not found
	 */
	@FXML void userQuit() throws IOException {
		serialize();
		System.exit(0);
	}
	
	/**Shows errors given the message to be displayed
	 * @param error message to be displayed
	 */
	public void showError(String error) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Error");
		alert.setHeaderText(error);
		alert.showAndWait();
	}
	
	/**
	 * This method is used for serializing data for users
	 * @throws IOException in case the saving file isn't found
	 */
	public void serialize() throws IOException {
		FileOutputStream fout=new FileOutputStream("users.ser");    
		ObjectOutputStream out=new ObjectOutputStream(fout);    
		out.writeObject(users);
		 
	}
}
