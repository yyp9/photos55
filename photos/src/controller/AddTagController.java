package controller;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Optional;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import model.Album;
import model.Photo;
import model.User;

/**
 * @author Yug Patel
 * @author Preston Peng
 * This class is the controller class for Add Tag Screen
 */
public class AddTagController {
	
	/**
	 * FXML ChoiceBox for Selecting previous tag type
	 */
	@FXML ChoiceBox<String> tagTypeChoiceBox; 
	/**
	 * FXML Textfield for Tag Value relating existing tag type
	 */
	@FXML TextField TagValue1TextField;
	/**
	 * FXML Textfiled for Tag Value relating new tag type
	 */
	@FXML TextField TagValue2TextField;
	/**
	 * FXML Textfield for entering new tag type
	 */
	@FXML TextField newTagType;

	
	/**
	 * The parent Album
	 */
	Album parent;
	/**
	 * List of users across the app on this device
	 */
	ArrayList<User> users;
	/**
	 * The current User on the app 
	 */
	User currUser;
	/**
	 * Tag types stored for the current user 
	 */
	ArrayList<String> userTagTypeList;
	/**
	 * The current photo for which tags are going to be added
	 */
	Photo currPhoto;
	/**
	 * This function starts the tag loading process by setting up preloaded tags
	 * in a choicebox and allowing for new tags to be entered
	 * @param mainStage the stage holding the scene
	 * @param curr the current album the photo is in
	 * @param userList list of users on the app
	 * @param p the current photo for tags to be added
	 * @param u the current user adding tag
	 */
	public void start(Stage mainStage, Album curr, ArrayList<User> userList, Photo p, User u) {
		this.parent = curr;
		this.users = userList;
		this.currUser = u;
		currPhoto = p;
		userTagTypeList = currUser.globalTagTypes;
		tagTypeChoiceBox.getItems().addAll(userTagTypeList);
		tagTypeChoiceBox.setValue(userTagTypeList.get(0));
	}
	
	/**
	 * This function is for canceling the adding tag process and going back to the main photos screen
	 * @param e actionevent variable
	 * @throws IOException in case screen is not found
	 */
	@FXML void cancel(ActionEvent e) throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/PhotosMainScreen.fxml"));
		Parent root = loader.load();
		AlbumController photosControl = loader.getController();
		Stage stage = (Stage)((Node)e.getSource()).getScene().getWindow();
		photosControl.start(stage, parent, currUser, users);
		stage.setScene(new Scene(root));
		stage.setTitle("Photos");
		stage.show();
	}
	
	/**
	 * Quits app instantly after saving
	 * @throws IOException in case file for serializaiton is not found
	 */
	@FXML void userQuit() throws IOException {
		serialize();
		System.exit(0);
	}
	
	/**
	 * This function enters a tag value for an existing tag type to the photo
	 * @param e ActionEvent e signaling the user's clicking of the button
	 * @throws IOException in case the photos screen isn't found
	 */
	@FXML void enterExisting(ActionEvent e) throws IOException{
		String val = TagValue1TextField.getText();
		if(val.length() < 1 || val == null || val.isBlank() || val.equals("")) {
			showError("Enter a tag value for selected tag type");
		}else {
			String tagType = tagTypeChoiceBox.getValue();
			String typeValue = tagType + val;
			if(currPhoto.tags.contains(typeValue)) {
				showError("Tag type with associated value already exists");
			}else {
				currPhoto.tags.add(typeValue);
			}
		}
		serialize();
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/PhotosMainScreen.fxml"));
		Parent root = loader.load();
		AlbumController photosControl = loader.getController();
		Stage stage = (Stage)((Node)e.getSource()).getScene().getWindow();
		photosControl.start(stage, parent, currUser, users);
		stage.setScene(new Scene(root));
		stage.setTitle("Photos");
		stage.show();
	}
	
	/**
	 * This functions enters a tag value for a new defined tag type to the photo
	 * @param e Action event
	 * @throws IOException in case the photos screen isn't found
	 */
	@FXML void enterNew(ActionEvent e) throws IOException {
		String newType = newTagType.getText();
		String val = TagValue2TextField.getText();
		if(val.length() < 1 || val == null || val.isBlank() || val.equals("")
				|| newType.length() < 1 || newType == null || newType.isBlank() || newType.equals("")) {
			showError("Enter a valid tag value/type");
		}else {

			String newTypeVal = newType + ":" + val;
			if(userTagTypeList.contains(newType)) {
				if(currPhoto.tags.contains(newTypeVal)) {
					showError("Tag type with associated value already exists");
				}else {
					currPhoto.tags.add(newTypeVal);
				}
			}else {
			if(currPhoto.tags.contains(newTypeVal)) {
				showError("Tag type with associated value already exists");
			}else {
				userTagTypeList.add(newType+":");
				currPhoto.tags.add(newTypeVal);
			}
			}
		}
		serialize();
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/PhotosMainScreen.fxml"));
		Parent root = loader.load();
		AlbumController photosControl = loader.getController();
		Stage stage = (Stage)((Node)e.getSource()).getScene().getWindow();
		photosControl.start(stage, parent, currUser, users);
		stage.setScene(new Scene(root));
		stage.setTitle("Photos");
		stage.show();
	}
	
	/**
	 * This method displays a dialog box with error
	 * @param error the message to be displayed in the dialog box
	 */
	public void showError(String error) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Error");
		alert.setHeaderText(error);
		alert.showAndWait();
	}
	
	/**
	 * This method is used for serializing data for users
	 * @throws IOException in case the saving file isn't found
	 */
	public void serialize() throws IOException {
		FileOutputStream fout=new FileOutputStream("users.ser");    
		ObjectOutputStream out=new ObjectOutputStream(fout);    
		out.writeObject(users);
		 
	}
}
