package controller;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Optional;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import model.Album;
import model.Photo;
import model.User;

/**
 * This class is the loader class for manual slideshow screen
 * This class allows for the easy switching between photos in the current album
 * @author Yug Patel
 * @author Preston Peng
 *
 */
public class SlideShowController {
	
	/**
	 * The imageview object holding the current image
	 */
	@FXML ImageView slideshowImg;
	/**
	 * The current album to which the images belong
	 */
	Album currAlbum;
	/**
	 * The index to keep track of which image is being displayed
	 */
	int index = 0;
	/**
	 * the user currently on the application
	 */
	User currUser;
	/**
	 * the list of users that are registered on this device and app
	 */
	ArrayList<User> users;
	/**
	 * This method loads up the first photo on the list on the designated imageview
	 * @param mainStage the stage where the scene is set
	 * @param a the current album in which all the photos in the slideshow belong
	 * @param u the current user on the app
	 * @param userList the list of users that are registered on this app on this device
	 */
	public void start(Stage mainStage, Album a, User u, ArrayList<User> userList) {
		this.currUser = u;
		this.currAlbum = a;
		this.users = userList;
		slideshowImg.setImage(new Image(currAlbum.photos.get(index).getLocation()));
	}

	/**
	 * Allows for going to the next photo, if the list has reached the end
	 * then starts back at the first photo
	 */
	@FXML void nextPhoto() {
		if(index == currAlbum.photos.size() - 1) {
			index = 0;
			slideshowImg.setImage(new Image(currAlbum.photos.get(index).getLocation()));
		}else {
			index++;
			slideshowImg.setImage(new Image(currAlbum.photos.get(index).getLocation()));
		}
	}
	
	/**
	 * Allows for going to the previous photo, if the list has reached the beginning
	 * then starts at the last photo at the end of the list
	 */
	@FXML void prevPhoto() {
		if(index == 0) {
			index = currAlbum.photos.size() - 1;
			slideshowImg.setImage(new Image(currAlbum.photos.get(index).getLocation()));
		}else {
			index--;
			slideshowImg.setImage(new Image(currAlbum.photos.get(index).getLocation()));
		}
	}
	
	/**
	 * This function allows for going back to the main photos screen that displays all
	 * the photos in the list in the current album
	 * @param e ActionEvent signaling the user's clicking of the button
	 * @throws IOException if the loader file is not found
	 */
	@FXML void backToPhotos(ActionEvent e) throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/PhotosMainScreen.fxml"));
		Parent root = loader.load();
		AlbumController photosControl = loader.getController();
		Stage stage = (Stage)((Node)e.getSource()).getScene().getWindow();
		photosControl.start(stage, currAlbum, currUser, users);
		stage.setScene(new Scene(root));
		stage.setTitle("Photos");
		stage.show();
	}
	/**
	 * Quits app instantly after saving
	 * @throws IOException in case file for serialization is not found
	 */
	@FXML void userQuit() throws IOException {
		serialize();
		System.exit(0);
	}
	/**
	 * This method is used for serializing data for users
	 * @throws IOException in case the saving file isn't found
	 */
	public void serialize() throws IOException {
		FileOutputStream fout=new FileOutputStream("users.ser");    
		ObjectOutputStream out=new ObjectOutputStream(fout);    
		out.writeObject(users);
	}
}
