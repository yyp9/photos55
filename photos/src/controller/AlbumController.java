package controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import java.io.ObjectOutputStream;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Optional;

import controller.AddTagController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import model.Album;
import model.Photo;
import model.User;

/**Controller for list of Photos under a given Album
 * @author Preston Peng
 * @author Yug Patel
 *
 */
public class AlbumController {

	/**
	 * The current album containing the photos
	 */
	private Album currAlbum;
	/**
	 * The current user browsing the photos
	 */
	private User albumUser;
	/**
	 * The FXML Button to add a new Photo to the current Album
	 */
	@FXML Button addPhotoButton;
	/**
	 * The FXML Photo ListView that shows photo captions with the photo thumbnails
	 */
	@FXML ListView<Photo> photoListView;
	/**
	 * The ObservableList that holds the Photo items to be displayed
	 */
	private ObservableList<Photo> obsList;
	/**
	 * The list of users registered on the app on this device
	 */
	public ArrayList<User> users = new ArrayList<User>();
	
	/**
	 * @param stage the current stage where the scene is set
	 * @param album the current Album containg the photos to be viewed
	 * @param u the current user on the app
	 * @param userList the list of users registered on the app on this device
	 */
	public void start(Stage stage, Album album, User u, ArrayList<User> userList) {
		users = userList;
		currAlbum = album;
		albumUser = u;
		obsList = FXCollections.observableArrayList();
		for(int i = 0; i < currAlbum.photos.size(); i++) {
			obsList.add(currAlbum.photos.get(i));
		}
		
		photoListView.setCellFactory(param -> new ListCell<Photo>() {
			@Override
			protected void updateItem(Photo p, boolean empty) {
				super.updateItem(p, empty);
				if(empty) {
					setGraphic(null);
				}else {
					HBox hBox = new HBox(5);
					hBox.setAlignment(Pos.CENTER_LEFT);
					ImageView img = null;
					try {
						img = new ImageView(new Image(new FileInputStream(p.getLocation())));
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						showError("Picture couldn't be loaded");
					}
					img.setFitHeight(80);
					img.setFitWidth(80);
					hBox.getChildren().addAll(new Label(p.getCaption() + "\t\t"), img);
					setGraphic(hBox);
				}
			}
			
		});
		photoListView.setItems(obsList);
	}
	
	/** The function for deleting the selected photo from the current album
	 * @throws IOException if the serializing file is not found
	 */
	@FXML void deletePhoto() throws IOException {
		int currIndex = photoListView.getSelectionModel().getSelectedIndex();
		if(currIndex < 0) {
			showError("Please select an item");
		}else {
			currAlbum.removePhoto(currIndex);
			obsList.remove(currIndex);
			photoListView.refresh();
		}
		serialize();
	}
	
	/** The function for adding a photo to the current album from the user's local system
	 * Also checks if the photo has already been added or not
	 * @throws IOException if the serializing file is not found
	 */
	@FXML void addNewPhoto() throws IOException {
		FileChooser fc = new FileChooser();
		fc.getExtensionFilters().addAll(new ExtensionFilter("Images", "*.jpeg", "*.png", "*.bmp", "*.jpg", "*.gif"));
		File selectedFile = fc.showOpenDialog(null);
		if(selectedFile != null) {
		String absPath = selectedFile.getAbsolutePath();
		String fileName = selectedFile.getName();
		boolean alreadyExists = false;
		for(int i = 0; i < currAlbum.photos.size(); i++) {
			if(currAlbum.photos.get(i).getLocation().equals(absPath)) {
				alreadyExists = true;
				break;
			}
		}
		if(alreadyExists) {
			showError("Photo with that name already exists");
		}else {
			Photo p = new Photo(absPath, fileName);
            File file = new File(absPath);
            long lastModDate = file.lastModified();
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(lastModDate);
            p.cal = cal;
			obsList.add(p);
			albumUser.globalPhotos.add(p);
			currAlbum.addNewPhoto(p);
			photoListView.refresh();
		}
	}
		serialize();
	}
	
	/**
	 * This functions opens up a separate display area for the user where they can see the details
	 * of the photo
	 * @param e ActionEvent e signaling the user's clicking of the button
	 * @throws IOException if loader file is not found
	 */
	@FXML void selectPhoto(ActionEvent e) throws IOException {
		int currIndex = photoListView.getSelectionModel().getSelectedIndex();
		if(currIndex < 0) {
			showError("Select a valid item");
		}else {
		Photo currPhoto = currAlbum.photos.get(currIndex);
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/PhotoDisplayScreen.fxml"));
		Parent root = loader.load();
		PhotosDisplayController displayControl = loader.getController();
		Stage stage = (Stage)((Node)e.getSource()).getScene().getWindow();
		displayControl.start(stage, currPhoto, albumUser, currAlbum, users);
		stage.setScene(new Scene(root));
		stage.setTitle("PhotosDisplay");
		stage.show();
		}
	}
	
	/**
	 * This functions opens up a separate display area for the user where they can manually click through photos
	 * in a slideshow manner
	 * @param e ActionEvent e signaling the user's clicking of the button
	 * @throws IOException if loader file is not found
	 */
	@FXML void openSlideShow(ActionEvent e) throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/PhotoSlideShowScreen.fxml"));
		Parent root = loader.load();
		SlideShowController slideshowControl = loader.getController();
		Stage stage = (Stage)((Node)e.getSource()).getScene().getWindow();
		slideshowControl.start(stage, currAlbum, albumUser, users);
		stage.setScene(new Scene(root));
		stage.setTitle("Login");
		stage.show();
	}
	
	/**
	 * This function opens a dialogbox to update the caption of a photo
	 * @throws IOException if serializing file is not found
	 */
	@FXML void updateCaption() throws IOException {
		int currIndex = photoListView.getSelectionModel().getSelectedIndex();
		if(currIndex < 0) {
			showError("Please select an item");
		}else {
			TextInputDialog dialog = new TextInputDialog("Update Caption");
			dialog.setTitle("Add/Change Caption");
			dialog.setHeaderText("Enter new caption: ");
			dialog.setContentText("Some Caption");
			Optional<String> result = dialog.showAndWait();
			result.ifPresent(name -> {
				if(name.length() < 1) {
					showError("Invalid Caption");
				}else {
					obsList.get(currIndex).setCaption(name);
					photoListView.refresh();
				}
			});
			}
		serialize();
		}
	
	/**
	 * Quits app instantly after saving
	 * @throws IOException in case file for serializaiton is not found
	 */
	@FXML void userQuit() throws IOException {
		serialize();
		System.exit(0);
	}
	
	/**serializes current list, and returns to login page
	 * @param e event when button is pressed
	 * @throws IOException if the loader file is not found
	 * @throws ClassNotFoundException if the loader class is not found
	 */
	@FXML void userLogout(ActionEvent e) throws IOException, ClassNotFoundException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/loginScreen.fxml"));
		Parent root = loader.load();
		LoginController loginControl = loader.getController();
		Stage stage = (Stage)((Node)e.getSource()).getScene().getWindow();
		loginControl.start(stage);
		stage.setScene(new Scene(root));
		stage.setTitle("Login");
		stage.show();
	}
	
	/**
	 * Opens up a choice dialog for a photo that shows tags associated with it
	 * that the user wants to remove.  Error if there are no tags
	 * @throws IOException if the serializing file is not found
	 */
	@FXML void removeTag() throws IOException {
		int currIndex = photoListView.getSelectionModel().getSelectedIndex();
		if(currIndex < 0) {
			showError("Select a valid item");
		}else {
			Photo curr = currAlbum.photos.get(currIndex);
			if(curr.tags.size() < 1) {
				showError("No tags to remove");
			}else {
			ChoiceDialog<String> cd = new ChoiceDialog<String>(curr.tags.get(0), curr.tags);
			cd.setTitle("Tags List: ");
			cd.setHeaderText("Select a Tag to remove");
			cd.setContentText("Tag: ");
			
			Optional<String> result = cd.showAndWait();
			result.ifPresent(tag -> {
				String res = result.get();
				curr.tags.remove(res);
			});
		}
		}
		serialize();
	}
	/**
	 * This function exits the screen displaying list of photos in the current album
	 * and returns back to the screen displaying all the albums that the user has
	 * @param e event when button is pressed
	 * @throws IOException if the loader file is not found
	 */
	@FXML void goBack(ActionEvent e) throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/albumScreen.fxml"));
		Parent root = loader.load();
		UserController userControl = loader.getController();
		Stage stage = (Stage)((Node)e.getSource()).getScene().getWindow();
		userControl.start(stage, albumUser, users);
		stage.setScene(new Scene(root));
		stage.setTitle(albumUser.getUserName() + "'s Albums");
		stage.show();
	}
	/**
	 * This function opens up a screen to add tags to a specific photo
	 * @param e event when button is pressed
	 * @throws IOException if the loader file is not found
	 */
	@FXML void addTag(ActionEvent e) throws IOException {
		
		int currIndex = photoListView.getSelectionModel().getSelectedIndex();
		if(currIndex < 0) {
			showError("Select a valid item");
		}else {
		Photo curr = currAlbum.photos.get(currIndex);
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/AddNewTagScreen.fxml"));
		Parent root = loader.load();
		AddTagController addTagControl = loader.getController();
		Stage stage = (Stage)((Node)e.getSource()).getScene().getWindow();
		addTagControl.start(stage, currAlbum, users, curr, albumUser);
		stage.setScene(new Scene(root));
		stage.setTitle("Login");
		stage.show();
		}
	}
	/**Shows errors given the message to be displayed
	 * @param error message to be displayed
	 */
	public void showError(String error) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Error");
		alert.setHeaderText(error);
		alert.showAndWait();
	}
	
	/**
	 * This method is used for serializing data for users
	 * @throws IOException in case the saving file isn't found
	 */
	public void serialize() throws IOException {
		FileOutputStream fout=new FileOutputStream("users.ser");    
		ObjectOutputStream out=new ObjectOutputStream(fout);    
		out.writeObject(users);
		 
	}
	
}
