package controller;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import model.Album;
import model.Photo;
import model.User;

/**
 * @author Yug Patel
 * @author Preston Peng
 * This class is the Controller class for the separate photo display screen area
 * 
 */
public class PhotosDisplayController {
	
	/**
	 * ListView that depicts the tags associated with the photo
	 */
	@FXML ListView<String> photoTagView;
	/**
	 * The fxml imageview object that will display the image 
	 */
	@FXML ImageView imgView;
	/**
	 * the datetime label that will show the date this picture was taken
	 */
	@FXML Label DateTime;
	/**
	 * The label that shows the caption of the picture associated with it
	 */
	@FXML Label captionLabel;
	/**
	 * the observable list holding items to be displayed
	 */
	private ObservableList<String> obsList;
	/**
	 * The current user on the app
	 */
	private User currUser;
	/**
	 * The album from which the photo came from
	 */
	Album parentAlbum;
	/**
	 * The list of users registered on the device
	 */
	public ArrayList<User> users = new ArrayList<User>();
	/**
	 * The current photo being displayed
	 */
	Photo currPhoto;
	
	/**
	 * This method loads up the display area by setting the image, date, caption, and tags appropriately
	 * @param mainStage the stage where the scene is set
	 * @param p the photo to be displayed
	 * @param u the user currently on the app
	 * @param parent the album to which the photo belongs
	 * @param userList the list of registered users on this app and device
	 */
	public void start(Stage mainStage, Photo p, User u, Album parent, ArrayList<User> userList) {
		parentAlbum = parent;
		users = userList;
		currPhoto = p;
		imgView.setImage(new Image(p.getLocation()));
		int month = currPhoto.cal.get(Calendar.MONTH) + 1;
		int day = currPhoto.cal.get(Calendar.DAY_OF_MONTH);
		int year = currPhoto.cal.get(Calendar.YEAR);
		String date = month + "-" + day + "-" + year;
		DateTime.setText(date);
		obsList = FXCollections.observableArrayList();
		captionLabel.setText(p.getCaption());
		currUser = u;
		for(int i = 0; i < p.tags.size(); i++) {
			obsList.add(p.tags.get(i));
		}
		photoTagView.setItems(obsList);
	}
	/**
	 * This method displays a dialog box with error
	 * @param error the message to be displayed in the dialog box
	 */
	public void showError(String error) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Error");
		alert.setHeaderText(error);
		alert.showAndWait();
	}
	/**
	 * Quits app instantly after saving
	 * @throws IOException in case file for serializaiton is not found
	 */
	@FXML void userQuit() throws IOException {
		serialize();
		System.exit(0);
	}
	
	/**
	 * This function allows users to move the displayed photo to another album and removing the photo
	 * from this album
	 * @param e ActionEvent signaling the user's clicking of the button
	 * @throws IOException if the serializing file is not found
	 */
	@FXML void moveToAlbum(ActionEvent e) throws IOException {
		ChoiceDialog<Album> d = new ChoiceDialog<Album>(currUser.albums.get(0), currUser.albums);
		d.setTitle("Album List");
		d.setHeaderText("Select an Album to move photo to");
		d.setContentText("Album: ");
		
		Optional<Album> result = d.showAndWait();
		result.ifPresent(album -> {
			boolean alreadyExists = false;
			Album currAb = result.get();
			for(int i = 0; i < currAb.photos.size(); i++) {
				if(currAb.photos.get(i).getLocation().equals(currPhoto.getLocation())) {
					alreadyExists = true;
					break;
				}
			}
			if(alreadyExists) {
				showError("Photo with that name already exists in that album");
			}else {
				currAb.photos.add(currPhoto);
				parentAlbum.photos.remove(currPhoto);
			}
		});
		serialize();
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/PhotosMainScreen.fxml"));
		Parent root = loader.load();
		AlbumController photosControl = loader.getController();
		Stage stage = (Stage)((Node)e.getSource()).getScene().getWindow();
		photosControl.start(stage, parentAlbum, currUser, users);
		stage.setScene(new Scene(root));
		stage.setTitle("Photos");
		stage.show();
	}
	
	/**
	 * This function allows users to copy the current photo from this album
	 * to another one
	 * @param e ActionEvent signaling the user's clicking of the button
	 * @throws IOException if the serializing file is not found
	 */
	@FXML void copyToAlbum(ActionEvent e) throws IOException {
		ChoiceDialog<Album> d = new ChoiceDialog<Album>(currUser.albums.get(0), currUser.albums);
		d.setTitle("Album List");
		d.setHeaderText("Select an Album to copy photo to");
		d.setContentText("Album: ");
		
		Optional<Album> result = d.showAndWait();
		result.ifPresent(album -> {
			boolean alreadyExists = false;
			Album currAb = result.get();
			for(int i = 0; i < currAb.photos.size(); i++) {
				if(currAb.photos.get(i).getLocation().equals(currPhoto.getLocation())) {
					alreadyExists = true;
					break;
				}
			}
			if(alreadyExists) {
				showError("Photo with that name already exists in that album");
			}else {
				currAb.photos.add(currPhoto);
			}
		});
		serialize();
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/PhotosMainScreen.fxml"));
		Parent root = loader.load();
		AlbumController photosControl = loader.getController();
		Stage stage = (Stage)((Node)e.getSource()).getScene().getWindow();
		photosControl.start(stage, parentAlbum, currUser, users);
		stage.setScene(new Scene(root));
		stage.setTitle("Photos");
		stage.show();
	}
	
	/**
	 * This function switches back to the screen displaying all the photos
	 * in the current album
	 * @param e ActionEvent signaling the user's clicking of the button
	 * @throws IOException if the loader file is not found
	 */
	@FXML void backToPhotos(ActionEvent e) throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/PhotosMainScreen.fxml"));
		Parent root = loader.load();
		AlbumController photosControl = loader.getController();
		Stage stage = (Stage)((Node)e.getSource()).getScene().getWindow();
		photosControl.start(stage, parentAlbum, currUser, users);
		stage.setScene(new Scene(root));
		stage.setTitle("Photos");
		stage.show();
	}
	/**
	 * This method is used for serializing data for users
	 * @throws IOException in case the saving file isn't found
	 */
	public void serialize() throws IOException {
		FileOutputStream fout=new FileOutputStream("users.ser");    
		ObjectOutputStream out=new ObjectOutputStream(fout);    
		out.writeObject(users);
		 
	}
}
