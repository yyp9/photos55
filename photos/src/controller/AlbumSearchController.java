package controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import model.Album;
import model.Photo;
import model.User;

	
/**
 * @author Yug Patel
 * @author Preston Peng
 *
 * This class functions as the controller class for the search screen of the app
 * This class allows users to search using multiple different filters
 */
public class AlbumSearchController implements java.io.Serializable{
	
	/**
	 * The choicebox for selecting tag type
	 */
	@FXML private ChoiceBox<String> tagType;
	/**
	 * Another choice box for selecting tag type when searching with two filters
	 */
	@FXML private ChoiceBox<String> tagTypeFirst;
	/**
	 * Another choice box for selecting tag type when searching with two filters
	 */
	@FXML private ChoiceBox<String> tagTypeSecond;
	/**
	 * A choicebox to select between the andOr option
	 */
	@FXML private ChoiceBox<String> andOr;
	/**
	 * The Textfield for beginning date range for range based search
	 */
	@FXML private TextField dateFirst;
	/**
	 * Another Textfield for beginning date range for range based search
	 */
	@FXML private TextField dateSecond;
	/**
	 * The Listview that will displays the photos that come from the search results
	 */
	@FXML ListView<Photo> photoSearchView;
	/**
	 * tag value field for searching by tag value
	 */
	@FXML private TextField tagValue;
	/**
	 * tag value field for doing 2 tag value type searches
	 */
	@FXML private TextField tagValueFirst;
	/**
	 * tag value field for doing 2 tag value type searches
	 */
	@FXML private TextField tagValueSecond;
	
	/**
	 * observable list that holds the photos to be displayed in listview
	 */
	private ObservableList<Photo> obsList;
	/**
	 * The user currently performing the search
	 */
	private User user;
	/**
	 * The list of users registered on the app on this device
	 */
	private ArrayList<User> users;

	/**
	 * This function starts up the search function by loading tag types
	 * in their respective choiceboxes
	 * @param stage the stage where the current scene is set
	 * @param currentUser the current user doing the search
	 * @param userList the list of users registered on the device and app
	 */
	public void start(Stage stage, User currentUser, ArrayList<User> userList) {
		user = currentUser;
		users = userList;
		
		tagType.getItems().addAll(user.globalTagTypes);
		tagTypeFirst.getItems().addAll(user.globalTagTypes);
		tagTypeSecond.getItems().addAll(user.globalTagTypes);
		andOr.getItems().add("and");
		andOr.getItems().add("or");
	}
	
	/**
	 * This function allows users to search photos by date given two dates for ranges
	 * @param e ActionEvent the event that signals the user clicking the button
	 * @throws ParseException if there is an error in parsing through the dates
	 */
	@FXML void searchDate(ActionEvent e) throws ParseException {
		obsList = FXCollections.observableArrayList();
        String firstDate = dateFirst.getText();
        String secondDate = dateSecond.getText();


        SimpleDateFormat curFormater = new SimpleDateFormat("dd/MM/yyyy"); 
        Date dateObj = curFormater.parse(firstDate); 
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateObj);

        SimpleDateFormat curFormater2 = new SimpleDateFormat("dd/MM/yyyy"); 
        Date dateObj2 = curFormater2.parse(secondDate); 
        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(dateObj2);

//        for(int i = 0; i < user.globalPhotos.size(); i++) {
//            if(calendar.compareTo(user.globalPhotos.get(i).cal) <= 0 && calendar2.compareTo(user.globalPhotos.get(i).cal) >= 0) {
//               obsList.add(user.globalPhotos.get(i));
//            }
//        }
        
        ArrayList<String> locs = new ArrayList<String>();
		for(int i = 0; i < user.albums.size(); i++) {
			for(int j = 0; j < user.albums.get(i).photos.size(); j++) {
					if(calendar.compareTo(user.albums.get(i).photos.get(j).cal) <= 0 && calendar2.compareTo(user.albums.get(i).photos.get(j).cal) >= 0) {
						String loc = user.albums.get(i).photos.get(j).getLocation();
						if(!locs.contains(loc)) {
							locs.add(loc);
							obsList.add(user.albums.get(i).photos.get(j));
							
						}
					}
				
			}
		}
        photoSearchView.setCellFactory(param -> new ListCell<Photo>() {
			@Override
			protected void updateItem(Photo p, boolean empty) {
				super.updateItem(p, empty);
				if(empty) {
					setGraphic(null);
				}else {
					HBox hBox = new HBox(5);
					hBox.setAlignment(Pos.CENTER_LEFT);
					ImageView img = null;
					try {
						img = new ImageView(new Image(new FileInputStream(p.getLocation())));
					}catch (FileNotFoundException e){
						showError("Picture couldn't be loaded");
					}
					img.setFitHeight(80);
					img.setFitWidth(80);
					hBox.getChildren().addAll(new Label(p.getCaption() + "\t\t"), img);
					setGraphic(hBox);
				}
			}
		});
		photoSearchView.setItems(obsList);
    }
	/**
	 * This function allows the users to search for photos based on one tag
	 * value pair
	 * @param e ActionEvent that signals the clicking of this button
	 */
	@FXML void searchOneTag(ActionEvent e) {
		obsList = FXCollections.observableArrayList();
		String tag = tagType.getValue();
		String tagVal = tagValue.getText();
		String t = tag + tagVal;
//		ArrayList<String> locs = new ArrayList<String>();
		
		for(int i = 0; i < user.albums.size(); i++) {
			for(int j = 0; j < user.albums.get(i).photos.size(); j++) {
				for(int z = 0; z < user.albums.get(i).photos.get(j).tags.size(); z++) {
					if(user.albums.get(i).photos.get(j).tags.get(z).equals(t)) {
						if(!obsList.contains(user.albums.get(i).photos.get(j))) {
							obsList.add(user.albums.get(i).photos.get(j));
						}
					}
				}
			}
		}
		photoSearchView.setCellFactory(param -> new ListCell<Photo>() {
			@Override
			protected void updateItem(Photo p, boolean empty) {
				super.updateItem(p, empty);
				if(empty) {
					setGraphic(null);
				}else {
					HBox hBox = new HBox(5);
					hBox.setAlignment(Pos.CENTER_LEFT);
					ImageView img = null;
					try {
						img = new ImageView(new Image(new FileInputStream(p.getLocation())));
					}catch (FileNotFoundException e){
						showError("Picture couldn't be loaded");
					}
					img.setFitHeight(80);
					img.setFitWidth(80);
					hBox.getChildren().addAll(new Label(p.getCaption() + "\t\t"), img);
					setGraphic(hBox);
				}
			}
		});
		photoSearchView.setItems(obsList);
	}
	
	
	/**
	 * This function allows users to search for photos by two tag value pairs
	 * either by and/or
	 * @param e ActionEvent signaling the clicking of this button
	 */
	@FXML void searchTwoTag(ActionEvent e) {
		obsList = FXCollections.observableArrayList();
		String firstTag = tagTypeFirst.getValue();
		String firstTagVal = tagValueFirst.getText();
		
		String secondTag = tagTypeSecond.getValue();
		String secondTagVal = tagValueSecond.getText();
		
		String first = firstTag + firstTagVal;
		String second = secondTag + secondTagVal;
		boolean firstVal = false;
		boolean secondVal = false;
		
		if(andOr.getValue().equals("and")) {
			for(int i = 0; i < user.albums.size(); i++) {
				for(int j = 0; j < user.albums.get(i).photos.size(); j++) {
					for(int z = 0; z < user.albums.get(i).photos.get(j).tags.size(); z++) {
						if(user.albums.get(i).photos.get(j).tags.get(z).equals(first)) {
							firstVal = true;
						}
						if(user.albums.get(i).photos.get(j).tags.get(z).equals(second)) {
							secondVal = true;
						}
					}
					
					if(firstVal && secondVal) {
						if(!obsList.contains(user.albums.get(i).photos.get(j))) {
						obsList.add(user.albums.get(i).photos.get(j));
						}
					}
					firstVal = false;
					secondVal = false;
				}
			}
		}
		else {
			for(int i = 0; i < user.albums.size(); i++) {
				for(int j = 0; j < user.albums.get(i).photos.size(); j++) {
					for(int z = 0; z < user.albums.get(i).photos.get(j).tags.size(); z++) {
						if(user.albums.get(i).photos.get(j).tags.get(z).equals(first)) {
							firstVal = true;
						}
						if(user.albums.get(i).photos.get(j).tags.get(z).equals(second)) {
							secondVal = true;
						}
					}

					if(firstVal || secondVal) {
						if(!obsList.contains(user.albums.get(i).photos.get(j))) {
						obsList.add(user.albums.get(i).photos.get(j));
						}
					}					
					firstVal = false;
					secondVal = false;
				}
			}
		}
		photoSearchView.setCellFactory(param -> new ListCell<Photo>() {
			@Override
			protected void updateItem(Photo p, boolean empty) {
				super.updateItem(p, empty);
				if(empty) {
					setGraphic(null);
				}else {
					HBox hBox = new HBox(5);
					hBox.setAlignment(Pos.CENTER_LEFT);
					ImageView img = null;
					try {
						img = new ImageView(new Image(new FileInputStream(p.getLocation())));
					}catch (FileNotFoundException e){
						showError("Picture couldn't be loaded");
					}
					img.setFitHeight(80);
					img.setFitWidth(80);
					hBox.getChildren().addAll(new Label(p.getCaption() + "\t\t"), img);
					setGraphic(hBox);
				}
			}
		});
		photoSearchView.setItems(obsList);

	}
	
	/**
	 * This method displays a dialog box with error
	 * @param error the message to be displayed in the dialog box
	 */
	public void showError(String error) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Error");
		alert.setHeaderText(error);
		alert.showAndWait();
	}
	
	
	/**
	 * This function allows users to make an album based on the search results
	 * from the filters
	 * @param e ActionEvent signaling the clicking of this button
	 * @throws IOException if the serializing file is not found
	 */
	@FXML void makeAlbum(ActionEvent e) throws IOException {
		TextInputDialog dialog = new TextInputDialog("New Album");
		dialog.setTitle("Create New Album");
		dialog.setHeaderText("Enter Album Name: ");
		dialog.setContentText("album");
		
		Optional<String> result = dialog.showAndWait();
		result.ifPresent(name -> {
			Album newAlbum = new Album(name);
			
			for(int i = 0; i < obsList.size(); i++) {
				newAlbum.addNewPhoto(obsList.get(i));
			}
			
			user.addNewAlbum(newAlbum);
			
		});
		serialize();
	}
	/**
	 * Quits app instantly after saving
	 * @throws IOException in case file for serializaiton is not found
	 */
	@FXML void quitApp(ActionEvent e) throws IOException {
		serialize();
		System.exit(0);
	}
	/**
	 * This function exits the search screen and takes the user back
	 * to the screen that lists all the albums
	 * @param e ActionEvent signaling the clicking of this button
	 * @throws IOException if the loader file is not found
	 */
	@FXML void exitSearch(ActionEvent e) throws IOException {
		serialize();
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/albumScreen.fxml"));
		Parent root = loader.load();
		UserController userControl = loader.getController();
		Stage stage = (Stage)((Node)e.getSource()).getScene().getWindow();
		User u = user;
		userControl.start(stage, u, users);
		stage.setScene(new Scene(root));
		stage.setTitle(u.getUserName() + "'s Albums");
		stage.show();
	}
	
	/**
	 * This method is used for serializing data for users
	 * @throws IOException in case the saving file isn't found
	 */
	public void serialize() throws IOException {
		FileOutputStream fout=new FileOutputStream("users.ser");    
		ObjectOutputStream out=new ObjectOutputStream(fout);    
		out.writeObject(users);
	}
	
}