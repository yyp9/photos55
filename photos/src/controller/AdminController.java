package controller;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Optional;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.stage.Stage;
import model.User;

/**Controller for the Admin page, with the ability to view, add and delete users
 * @author Preston Peng
 * @author Yug Patel
 *
 */
public class AdminController implements java.io.Serializable{
	
	/**
	 * The ListView List that displays contents
	 */
	@FXML ListView<String> userListView;
	/**
	 * the observable list that holds items to be displayed
	 */
	private ObservableList<String> obsList;
	/**
	 * the list of users that use this app on this device
	 */
	private ArrayList<User> users;
	/**Starts the admin page, and loads in userList
	 * @param mainStage loads in main stage
	 * @param userList list of all users
	 */
	public void start(Stage mainStage, ArrayList<User> userList) {
		users = userList;
		obsList = FXCollections.observableArrayList();
		for(int i = 0; i < users.size(); i++) {
			obsList.add(users.get(i).getUserName());
		}
		userListView.setItems(obsList);

	}
	
	/**Adds user to the users list, and checks to make sure they aren't an existing user
	 * @throws IOException 
	 * 
	 */
	@FXML void addUser() throws IOException {
		TextInputDialog dialog = new TextInputDialog("New User");
		dialog.setTitle("Create New User");
		dialog.setHeaderText("Enter username: ");
		dialog.setContentText("username");
		
		Optional<String> result = dialog.showAndWait();
		result.ifPresent(name -> {
			if(!obsList.contains(name)) {
				obsList.add(name);
				users.add(new User(name));
			}else {
				showError("User already exists");
			}
		});
		serialize();
	}
	
	/**Shows errors given the message to be displayed
	 * @param error message to be displayed
	 */
	public void showError(String error) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Error");
		alert.setHeaderText(error);
		alert.showAndWait();
	}
	
	/**Removes the selected user from the list
	 * @throws IOException if the serialization file is not found
	 * 
	 */
	@FXML void deleteUser() throws IOException {
		int currIndex = userListView.getSelectionModel().getSelectedIndex();
		if(currIndex < 0) {
			showError("Please select an item");
		}else {
			obsList.remove(currIndex);
			users.remove(currIndex);
		}
		serialize();
	}
	
	/**serializes current list, and returns to login page
	 * @param e event when button is pressed
	 * @throws IOException if the loader file is not found
	 * @throws ClassNotFoundException if the loader class is not found
	 */
	@FXML void userLogout(ActionEvent e) throws IOException, ClassNotFoundException {
		serialize();
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/loginScreen.fxml"));
		Parent root = loader.load();
		LoginController loginControl = loader.getController();
		Stage stage = (Stage)((Node)e.getSource()).getScene().getWindow();
		loginControl.start(stage);
		stage.setScene(new Scene(root));
		stage.setTitle("Login");
		stage.show();
	}
	


	/**exit program after saving data
	 * @throws IOException exception for file not found
	 * 
	 */
	@FXML void userQuit() throws IOException {
		serialize();
		System.exit(0);
	}
	
	/**Serializes user list
	 * @throws IOException
	 */
	public void serialize() throws IOException {
		 FileOutputStream fout=new FileOutputStream("users.ser");    
		 ObjectOutputStream out=new ObjectOutputStream(fout);    
		 out.writeObject(users);
	}
	
}
