package app;

import controller.LoginController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * The driver class for the photo application, allowing users to create albums, add photos, add tags, and edit content
 * @author Preston Peng
 * @author Yug Patel
 * 
 */

public class Photos extends Application {

	/**
	 *Starts the UI by bringing up the login screen
	 */
	@Override
	public void start(Stage primaryStage) throws Exception {
		// TODO Auto-generated method stub
		FXMLLoader loader = new FXMLLoader();   
		loader.setLocation(
				getClass().getResource("/view/loginScreen.fxml"));
		AnchorPane root = (AnchorPane)loader.load();

		LoginController loginController = 
				loader.getController();


		Scene scene = new Scene(root, 600, 400);		
		loginController.start(primaryStage);
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	/**
	 * The main method, that calls upon the launch method
	 * @param args the default arguments from the console if any at all
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		launch(args);
	}

}
